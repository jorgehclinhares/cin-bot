angular.module('usainBot').directive('chatMensagem', function($compile) {
    return {
      template: '',
      replace: true,
      link: function(scope, element) {
        var el = angular.element('<span/>');
        switch(scope.mensagem.tipo) {
          case 'user':
            var html ='<div class="containerMensagem row animated fadeIn">';
              html = html + '<div class="col-xs-12 no-padding">';
                html = html + '<p class="mensagem user pull-right">';
                  html = html + "<spam class='id'>Você:</spam><br>";
                  html = html + scope.mensagem.texto;
                html = html + '</p>';
              html = html + '</div>';
            html = html + '</div>';
            el.append(html);
            break;
          case 'usainbot':
            var html ='<div class="containerMensagem row animated fadeIn">';
              html = html + '<div class="col-xs-10 no-padding">';
                html = html + '<p class="mensagem usainbot">';
                  html = html + "<spam class='id'>CIn bot:</spam><br>";
                  html = html + scope.mensagem.texto;
                html = html + '</p>';
              html = html + '</div>';
            html = html + '</div>';
            el.append(html);
            break;
          case 'usainbot-noticias':
            var html ='<div class="containerMensagem row animated fadeIn">';
              html = html + '<div class="col-xs-10 no-padding">';
                html = html + '<p class="mensagem usainbot">';
                  html = html + "<spam class='id'>CIn bot:</spam><br>";
                  html = html + "<b>"+scope.mensagem.titulo+"</b>";
                  html = html + "<br>";
                  html = html + "<a href='" + scope.mensagem.uri + "' class='btn btn-default btn-sm' target='_blank'> Mais Informações</a>";
                html = html + '</p>';
              html = html + '</div>';
            html = html + '</div>';
            el.append(html);
            break;
            case 'usainbot-eventos':
              var html ='<div class="containerMensagem row animated fadeIn">';
                html = html + '<div class="col-xs-10 no-padding">';
                  html = html + '<p class="mensagem usainbot">';
                    html = html + "<spam class='id'>CIn bot:</spam><br>";
                    html = html + "<b>" + scope.mensagem.data + " - " + scope.mensagem.titulo+"</b>";
                    html = html + "<br>";
                    html = html + "<a href='" + scope.mensagem.uri + "' class='btn btn-default btn-sm' target='_blank'> Mais Informações</a>";
                  html = html + '</p>';
                html = html + '</div>';
              html = html + '</div>';
              el.append(html);
              break;
        }
        $compile(el)(scope);
        element.append(el);
      }
    }
  });
