angular.module('usainBot',['ng','ngAnimate','ngAria','ngMaterial','ngRoute','luegg.directives']);

/**
* Rotas
*/
angular.module('usainBot').config(function($routeProvider, $locationProvider) {

    $routeProvider.when("/chat", {
        templateUrl : "app/views/chat.html",
        controller: "Chat"
    }).otherwise({
        redirectTo: '/chat'
    });

});

/**
* Constantes
*/
angular.module('usainBot').constant("CONFIG", {
  "app" : {
    "host" : "http://api.jorgelinhares.space",
    "id" : "589520c2f0ed6f35227495c5",
    "secret": "BO9IT25E11ZLSUUF9TP9MYXTFVSKSK8T90",
    "bot": {
      "id" : "58952138f0ed6f35227495c6"
    }
  }
});


angular.module('usainBot').provider('config', function () {
    var options = {};
    this.config = function (opt) {
        angular.extend(options, opt);
    };
    this.$get = [function () {
        if (!options) {
            throw new Error('Config options must be configured');
        }
        return options;
    }];
});
