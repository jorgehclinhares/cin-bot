(function() {

    'use strict';

    angular.module('usainBot').controller('Chat', chatController);

    chatController.$inject = ['$scope', '$sce', '$timeout','CONFIG', '$http','$location', '$window'];

    function chatController ($scope, $sce, $timeout, CONFIG, $http, $location, $window) {

      atualizarToken();

      $scope.token = null;

      $scope.urls = [
        { uri: '/', nome: 'Chat' },
        { uri: 'http://docs.usainbot.apiary.io/', nome: 'Documentação', newtab: true }
      ];

      $scope.openUrl = function(url) {
        if(url.newtab) {
          $window.open(url.uri, '_blank');
        } else {
          $location.path(url.uri);
        }
      };

      $scope.openUrlNewTab = function(elemento){
        $window.open(elemento.uri, '_blank');
      }

      $scope.glued = true;
      $scope.input = { mensagem: null };
      var bloqueiaEnvio = false;

      $scope.enviarMensagem = function (mensagem) {

        if (!mensagem) {
          if($scope.input.mensagem && $scope.input.mensagem == ''){
            return;
          }
        }

        if(bloqueiaEnvio) return;

        bloqueiaEnvio = true;

        $scope.mensagens.push({ tipo: 'user', texto: $scope.input.mensagem || mensagem });

        consultar($scope.input.mensagem || mensagem, function(err, dados){

          $scope.input.mensagem = null;

          if (err) return;

          if(dados.tipo){
            if(dados.tipo == "noticias"){
              var noticias = [];
              for (var i = 0; i < dados.noticias.length; i++) {
                noticias.push({ titulo: dados.noticias[i].titulo, resumo: dados.noticias[i].resumo, uri: dados.noticias[i].uri });
              }
              $scope.mensagens.push({
                tipo: 'cinbot-noticias',
                noticias : noticias
              });
            } else if(dados.tipo == "eventos") {
              var eventos = [];
              for (var i = 0; i < dados.eventos.length; i++) {
                var data = new Date(dados.eventos[i].data);
                data = data.getDay() + "/" + data.getMonth();
                eventos.push({ tipo: 'cinbot-eventos', titulo: dados.eventos[i].titulo, data: data, uri: dados.eventos[i].uri });
              }
              $scope.mensagens.push({
                tipo: 'cinbot-eventos',
                eventos : eventos
              });
            }
          }
          else {
            $scope.mensagens.push({ tipo: 'cinbot', texto: dados.resposta.word || dados.resposta, sugestoes: dados.sugestoes || [] });
          }
          bloqueiaEnvio = false;

        });

      }

      // Consulta a API
      function consultar (mensagem, done) {

        $http({
          method: 'GET',
          url: CONFIG.app.host + '/v1.0/bot/' + CONFIG.app.bot.id + '/consultar/?q=' + mensagem,
          headers: {
            'Content-Type' : 'application/json',
            'Authorization': $scope.token
          }
        }).then(function successCallback(res) {

          if(res.status = 200) {
            done(false, res.data);
          } else {
            done(true);
          }

        }, function errorCallback(res) {
          done(true);
        });

      }

      function atualizarToken (done) {

        $http({
          method: 'PUT',
          url: CONFIG.app.host + '/v1.0/app/' + CONFIG.app.id + '/atualizar/token',
          headers: {
            'Content-Type' : 'application/json'
          },
          data: {
            dados:{
              'secret' : CONFIG.app.secret
            }
          }
        }).then(function successCallback(res) {

          $scope.token = "Bearer " + res.data.dados.token;

          consultar('abertura', function(err, dados){
            if(err) return;
            $scope.mensagens = [{ tipo: 'cinbot', texto: dados.resposta }];
          });

        }, function errorCallback(res) {
          alert('Não foi possível identificar este bot');
        });

      }

    }

})();
